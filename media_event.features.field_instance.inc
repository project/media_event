<?php
/**
 * @file
 * media_event.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function media_event_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'media_event-media_event-field_media_event_deleted'.
  $field_instances['media_event-media_event-field_media_event_deleted'] = array(
    'bundle' => 'media_event',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'If the media event is deleted.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'media_event',
    'field_name' => 'field_media_event_deleted',
    'label' => 'Deleted',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'media_event-media_event-field_media_event_end'.
  $field_instances['media_event-media_event-field_media_event_end'] = array(
    'bundle' => 'media_event',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'hms_field',
        'settings' => array(
          'format' => 'h:mm',
          'leading_zero' => TRUE,
        ),
        'type' => 'hms_default_formatter',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'media_event',
    'field_name' => 'field_media_event_end',
    'label' => 'End',
    'required' => 1,
    'settings' => array(
      'default_description' => 1,
      'format' => 'h:mm:ss',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'hms_field',
      'settings' => array(),
      'type' => 'hms_default_widget',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'media_event-media_event-field_media_event_media_ref'.
  $field_instances['media_event-media_event-field_media_event_media_ref'] = array(
    'bundle' => 'media_event',
    'default_value' => NULL,
    'default_value_function' => 'entityreference_prepopulate_field_default_value',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'media_event',
    'field_name' => 'field_media_event_media_ref',
    'label' => 'Media',
    'required' => 0,
    'settings' => array(
      'behaviors' => array(
        'prepopulate' => array(
          'action' => 'hide',
          'action_on_edit' => 1,
          'fallback' => 'none',
          'providers' => array(
            'og_context' => FALSE,
            'url' => 1,
          ),
          'skip_perm' => 0,
          'status' => 1,
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'media_event-media_event-field_media_event_start'.
  $field_instances['media_event-media_event-field_media_event_start'] = array(
    'bundle' => 'media_event',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'hms_field',
        'settings' => array(
          'format' => 'h:mm',
          'leading_zero' => TRUE,
        ),
        'type' => 'hms_default_formatter',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'media_event',
    'field_name' => 'field_media_event_start',
    'label' => 'Start',
    'required' => 0,
    'settings' => array(
      'default_description' => 1,
      'format' => 'h:mm:ss',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'hms_field',
      'settings' => array(),
      'type' => 'hms_default_widget',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'media_event-media_event-field_media_event_text'.
  $field_instances['media_event-media_event-field_media_event_text'] = array(
    'bundle' => 'media_event',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'media_event',
    'field_name' => 'field_media_event_text',
    'label' => 'Text',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'media_event-media_event-field_media_event_title'.
  $field_instances['media_event-media_event-field_media_event_title'] = array(
    'bundle' => 'media_event',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'media_event',
    'field_name' => 'field_media_event_title',
    'label' => 'Title',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ds_code' => 'ds_code',
          'html' => 'html',
          'plain_text' => 'plain_text',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ds_code' => array(
              'weight' => 12,
            ),
            'html' => array(
              'weight' => 0,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
          ),
        ),
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Deleted');
  t('End');
  t('If the media event is deleted.');
  t('Media');
  t('Start');
  t('Text');
  t('Title');

  return $field_instances;
}

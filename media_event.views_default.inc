<?php
/**
 * @file
 * media_event.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function media_event_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'media_events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'media_event';
  $view->human_name = 'Media Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Media Events';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'media_event_id' => 'media_event_id',
    'field_media_event_media_ref' => 'field_media_event_media_ref',
    'field_media_event_title' => 'field_media_event_title',
    'nothing' => 'nothing',
    'field_media_event_end' => 'field_media_event_end',
    'field_media_event_start' => 'field_media_event_start',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'media_event_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_media_event_media_ref' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_media_event_title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_media_event_end' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_media_event_start' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<a href="/media-event/add">Add Media Event</a>';
  $handler->display->display_options['header']['area']['format'] = 'ds_code';
  /* Field: Media Event: Media event ID */
  $handler->display->display_options['fields']['media_event_id']['id'] = 'media_event_id';
  $handler->display->display_options['fields']['media_event_id']['table'] = 'media_event';
  $handler->display->display_options['fields']['media_event_id']['field'] = 'media_event_id';
  $handler->display->display_options['fields']['media_event_id']['exclude'] = TRUE;
  /* Field: Media Event: Media */
  $handler->display->display_options['fields']['field_media_event_media_ref']['id'] = 'field_media_event_media_ref';
  $handler->display->display_options['fields']['field_media_event_media_ref']['table'] = 'field_data_field_media_event_media_ref';
  $handler->display->display_options['fields']['field_media_event_media_ref']['field'] = 'field_media_event_media_ref';
  $handler->display->display_options['fields']['field_media_event_media_ref']['settings'] = array(
    'link' => 0,
  );
  /* Field: Media Event: Title */
  $handler->display->display_options['fields']['field_media_event_title']['id'] = 'field_media_event_title';
  $handler->display->display_options['fields']['field_media_event_title']['table'] = 'field_data_field_media_event_title';
  $handler->display->display_options['fields']['field_media_event_title']['field'] = 'field_media_event_title';
  /* Field: Media Event: Start */
  $handler->display->display_options['fields']['field_media_event_start']['id'] = 'field_media_event_start';
  $handler->display->display_options['fields']['field_media_event_start']['table'] = 'field_data_field_media_event_start';
  $handler->display->display_options['fields']['field_media_event_start']['field'] = 'field_media_event_start';
  $handler->display->display_options['fields']['field_media_event_start']['settings'] = array(
    'format' => 'h:mm',
    'leading_zero' => 1,
  );
  /* Field: Media Event: End */
  $handler->display->display_options['fields']['field_media_event_end']['id'] = 'field_media_event_end';
  $handler->display->display_options['fields']['field_media_event_end']['table'] = 'field_data_field_media_event_end';
  $handler->display->display_options['fields']['field_media_event_end']['field'] = 'field_media_event_end';
  $handler->display->display_options['fields']['field_media_event_end']['settings'] = array(
    'format' => 'h:mm',
    'leading_zero' => 1,
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'media-event/[media_event_id]/edit';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'media-events';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Media Events';
  $handler->display->display_options['menu']['weight'] = '0';
  $translatables['media_events'] = array(
    t('Master'),
    t('Media Events'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('<a href="/media-event/add">Add Media Event</a>'),
    t('Media event ID'),
    t('.'),
    t(','),
    t('Media'),
    t('Title'),
    t('Start'),
    t('End'),
    t('Edit'),
    t('Page'),
  );
  $export['media_events'] = $view;

  return $export;
}
